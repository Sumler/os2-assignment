//
// Created by daniel on 7/1/20.
//
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>

void server(){

    struct sockaddr_in saddr, caddr;
    int sockfd, clen, isock;
    unsigned short port = 80;

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("Error creating socket\n");
    }


    bzero((char *)&saddr, sizeof(saddr));  	    // zero structure out
    saddr.sin_family = AF_INET;		   		    // match the socket() call
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);	// bind to any local address
    saddr.sin_port = htons(port);				// specify port to listen on

    if(bind(sockfd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) { // bind!
        printf("Error binding\n");
    }
    if(listen(sockfd, 5) < 0) {        // listen for incoming connections
        printf("Error listening\n");
    }
    // accept one
    clen=sizeof(caddr);
    if((isock = accept(sockfd, (struct sockaddr *) &caddr, &clen)) < 0) {
        printf("Error accepting\n");

    }

}